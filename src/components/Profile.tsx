import React from 'react'
import ProfileData from "../data/ProfileData";

interface IProps {
    profile: ProfileData;
}

export class Profile extends React.Component<IProps> {
    render() {
        const {name, email, picture} = this.props.profile;
        return (
            <div>
                <p>Hi, {name.first} {email} !</p>
                <img src={picture.large} alt={email}/>
            </div>
        )
    }
}
