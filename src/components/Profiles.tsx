import React from 'react';
import styled from 'styled-components';
import ProfileData from "../data/ProfileData";

const DivProfileList = styled.div`
    border: none;
    outline: none;    
     height:200px;
    width:200px;   
}
`;

interface IProps {
    profiles: ProfileData[];
    getProfile: (profile: ProfileData) => void;
}

class PersonListFetch extends React.Component<IProps> {
    render() {
        const {profiles, getProfile} = this.props;

        return (
            <DivProfileList>
                <h1>Fetch</h1>
                <ul>
                    <ul>
                        {profiles.map((person: ProfileData, i: number) =>
                            <li key={i}><img alt={person.email} onClick={() => getProfile(person)}
                                             src={person.picture.large}/>{person.name.first} : <b>{person.email}</b>
                            </li>)}
                    </ul>
                </ul>
            </DivProfileList>
        );
    }
}

export default PersonListFetch;
