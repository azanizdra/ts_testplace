import {GET_PROFILES_REQUEST} from "../actions/ProfilesAction";

const initialState = {
    people: [],
    isFetching: false,
}
export default (state = initialState, action: any): any => {

    switch (action.type) {
        case GET_PROFILES_REQUEST:
            return { ...state, people: action.payload, isFetching: true }

        default:
            return state
    }
}
