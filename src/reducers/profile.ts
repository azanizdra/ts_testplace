import {GET_PROFILES_REQUEST} from "../actions/ProfileAction";
import ProfileData from "../data/ProfileData";

const initialState = new ProfileData(
    {first: 'name'},
    'test@mail.com',
    {large: 'https://randomuser.me/api/portraits/men/47.jpg'}
);

export default (state: ProfileData = initialState, action: any): any => {
    switch (action.type) {
        case GET_PROFILES_REQUEST:
            return {...state, ...action.payload}

        default:
            return state
    }
}