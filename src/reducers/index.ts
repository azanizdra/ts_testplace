import {combineReducers} from 'redux'

import profile from './profile'
import profiles from './profiles'

export default  combineReducers({
    profile,
    profiles
})