import React from 'react';
import {connect} from 'react-redux'
import './App.css';
import {Profile} from './components/Profile'
import Profiles from './components/Profiles'
import ProfileData from "./data/ProfileData";
import getProfiles from './actions/ProfilesAction'
import getProfile from './actions/ProfileAction'

interface IProps {
  profile: ProfileData;
  profiles: ProfileData[];
    getProfile: (profile: ProfileData) => void;
  getProfilesAction: () => void;
}


class App extends React.Component<IProps> {
    componentDidMount() {
        this.props.getProfilesAction() //
    }

    public render() {
    const { profile, profiles, getProfile} = this.props;

    return (
        <div className="App">

            <Profiles profiles={profiles} getProfile={getProfile} />
            <Profile profile={profile}/>
        </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    profiles: state.profiles.people,
    profile: state.profile
  }
}
const mapDispatchToProps = (dispatch: any) => ({
    getProfilesAction: () => dispatch(getProfiles()),
    getProfile: (profile:ProfileData) => dispatch(getProfile(profile)),

})

export default connect(mapStateToProps, mapDispatchToProps)(App)
