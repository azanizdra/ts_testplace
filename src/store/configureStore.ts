import {createStore as create, applyMiddleware, compose} from 'redux'
import rootReducer from '../reducers'
//import logger from 'redux-logger'
import thunk from 'redux-thunk'
import {composeWithDevTools} from 'redux-devtools-extension';


const createStore = () => {
    const middlewares = [thunk];
    const enhancers: any[] = [];

    const store = create(
        rootReducer,
        compose(
            composeWithDevTools(
                applyMiddleware(...middlewares),
                ...enhancers
            )
        )
    );

    return store;
};

export default createStore();