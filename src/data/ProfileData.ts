class ProfileData {
    constructor(public name: {first: string;}, public email: string, public  picture: {large: string;}) {
    }
}

export default ProfileData;