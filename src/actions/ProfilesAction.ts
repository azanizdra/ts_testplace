export const GET_PROFILES_REQUEST = 'GET_PROFILES_REQUEST'

const getProfiles = () => {
    return  async (dispatch: any) => {
        const url: string = 'https://randomuser.me/api/?results=10&noinfo';
        let data =  await fetch(url).then(resp => resp.json()).then(json => json.results);

        dispatch({ type: GET_PROFILES_REQUEST, payload: data });
    }
};

export default getProfiles;
