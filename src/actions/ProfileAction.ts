import ProfileData from "../data/ProfileData";

export const GET_PROFILES_REQUEST = 'GET_PROFILE_REQUEST'

export default function getProfile(person: ProfileData) {

    return {
        type: GET_PROFILES_REQUEST,
        payload: person,
    }
}